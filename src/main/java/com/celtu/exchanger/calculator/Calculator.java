package com.celtu.exchanger.calculator;

import com.celtu.exchanger.exception.ExchangerException;
import com.celtu.exchanger.exception.ExchangerIllegalArgumentException;
import com.celtu.exchanger.tables.ExchangeTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

@Component
public class Calculator {

    @Autowired
    private ExchangeTable exchangeTable;

    public BigDecimal recalculate(String currencyCode, BigDecimal amount, String targetCurrencyCode) {
        Currency currency = null;
        try {
            currency = Currency.getInstance(currencyCode);
        } catch (IllegalArgumentException e) {
            throw new ExchangerIllegalArgumentException("Invalid currency code: " + currencyCode, e);
        }
        Currency targetCurrency = null;
        try {
            targetCurrency = Currency.getInstance(targetCurrencyCode);
        } catch (IllegalArgumentException e) {
            throw new ExchangerIllegalArgumentException("Invalid target currency code: "  + targetCurrencyCode, e);
        }
        return recalculate(currency, amount, targetCurrency);
    }

    public BigDecimal recalculate(Currency currency, BigDecimal amount, Currency targetCurrency) {
        BigDecimal rate = exchangeTable.getRate(currency);
        if (rate == null) {
            throw new ExchangerException("Unknown rates for currency: " + currency.getCurrencyCode());
        }
        BigDecimal targetRate = exchangeTable.getRate(targetCurrency);
        if (targetRate == null) {
            throw new ExchangerException("Unknown rates for target currency: " + targetCurrency.getCurrencyCode());
        }
        BigDecimal amountPLN = amount.multiply(rate);
        BigDecimal amountTarget = amountPLN.divide(targetRate, 2, RoundingMode.HALF_UP);
        return amountTarget;
    }
}
