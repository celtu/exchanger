package com.celtu.exchanger.tables;

import com.celtu.exchanger.exception.ExchangerException;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ExchangeTable {

    @Getter
    private Map<Currency, BigDecimal> table;

    public BigDecimal getRate(Currency currency) {
        if (table == null) {
            throw new ExchangerException("Exchange table not yet initialized!");
        }
        if ("PLN".equals(currency.getCurrencyCode())) {
            return BigDecimal.ONE;
        }
        return table.get(currency);
    }

    public void putAll(Map<Currency, BigDecimal> map) {
        if (table == null) {
            table = new ConcurrentHashMap<>();
        }
        table.putAll(map);
    }

    public void clear() {
        table.clear();
    }

}
