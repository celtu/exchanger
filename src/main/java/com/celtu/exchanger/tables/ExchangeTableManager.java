package com.celtu.exchanger.tables;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

@Service
public class ExchangeTableManager {

    @Autowired
    private ExchangeTable exchangeTable;

    public void merge(Map<Currency, BigDecimal> mergeWithTable) {
        this.exchangeTable.putAll(mergeWithTable);
    }

}
