package com.celtu.exchanger.tables;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@ConditionalOnProperty(name = "exchanger.table.update.scheduler")
public class ExchangeTableScheduler {

    @Autowired
    private ExchangeTablesUpdater exchangeTablesUpdater;

    @Scheduled(fixedRate = 86400000l)
    public void updateExchangeRates() {
        log.info("Scheduled update of exchange rates");
        exchangeTablesUpdater.update();
    }
}
