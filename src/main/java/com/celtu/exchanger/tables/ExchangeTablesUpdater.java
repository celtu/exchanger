package com.celtu.exchanger.tables;

import com.celtu.exchanger.nbp.NBPRatesLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

@Service
public class ExchangeTablesUpdater {

    @Autowired
    private NBPRatesLoader loader;

    @Autowired
    private ExchangeTableManager manager;

    // 1st beautiful version :D
    public void update() {
        Map<Currency, BigDecimal> tableA = loader.loadTable("A");
        manager.merge(tableA);
        Map<Currency, BigDecimal> tableB = loader.loadTable("B");
        manager.merge(tableB);
    }
}
