package com.celtu.exchanger.exception;

public class ExchangerException extends RuntimeException {
    public ExchangerException() {
    }

    public ExchangerException(String message) {
        super(message);
    }

    public ExchangerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExchangerException(Throwable cause) {
        super(cause);
    }

    public ExchangerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
