package com.celtu.exchanger.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExchangerIllegalArgumentException extends ExchangerException {
    public ExchangerIllegalArgumentException() {
    }

    public ExchangerIllegalArgumentException(String message) {
        super(message);
    }

    public ExchangerIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ExchangerIllegalArgumentException(Throwable cause) {
        super(cause);
    }

    public ExchangerIllegalArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
