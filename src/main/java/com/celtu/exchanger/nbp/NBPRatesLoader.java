package com.celtu.exchanger.nbp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NBPRatesLoader {

    @Autowired
    private RestTemplate restTemplate;

    public Map<Currency, BigDecimal> loadTable(String table) {
        ResponseEntity<JsonNode> response = restTemplate.getForEntity("http://api.nbp.pl/api/exchangerates/tables/" + table, JsonNode.class);
        JsonNode rates = response.getBody().get(0).get("rates");
        Map<Currency, BigDecimal> result = new HashMap<>();
        for (JsonNode rate : rates) {
            String code = rate.get("code").asText();
            BigDecimal value = rate.get("mid").decimalValue();
            result.put(Currency.getInstance(code), value);
        }
        return result;
    }

}
