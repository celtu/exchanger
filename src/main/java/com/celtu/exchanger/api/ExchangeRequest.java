package com.celtu.exchanger.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Currency;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExchangeRequest {

    @NotBlank
    public String currencyCode;

    @NotNull
    public BigDecimal amount;

    @NotBlank
    public String targetCurrencyCode;

}
