package com.celtu.exchanger.api;

import com.celtu.exchanger.calculator.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping(path = "/api/exchanger/")
public class ExchangeEndpoint {

    @Autowired
    private Calculator calculator;

    @RequestMapping(method = RequestMethod.POST, path = "")
    public BigDecimal exchange(@Validated @RequestBody ExchangeRequest exchangeRequest) {
        return calculator.recalculate(exchangeRequest.getCurrencyCode(), exchangeRequest.getAmount(), exchangeRequest.getTargetCurrencyCode());
    }
}
