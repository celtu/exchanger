package com.celtu.exchanger.calculator;

import com.celtu.exchanger.exception.ExchangerIllegalArgumentException;
import com.celtu.exchanger.tables.ExchangeTable;
import com.celtu.exchanger.tables.ExchangeTableManager;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
public class CalculatorTest {

    @Autowired
    private Calculator calculator;

    @Autowired
    private ExchangeTableManager exchangeTableManager;

    @Autowired
    private ExchangeTable exchangeTable;

    @Before
    public void init() {
        Map<Currency, BigDecimal> rates = new HashMap<>();
        rates.put(Currency.getInstance("EUR"), BigDecimal.valueOf(4.5234));
        rates.put(Currency.getInstance("USD"), BigDecimal.valueOf(4.1503));
        rates.put(Currency.getInstance("GBP"), BigDecimal.valueOf(5.0538));
        exchangeTableManager.merge(rates);
    }

    @Test
    public void shouldConvertUSDtoEUR() {
        BigDecimal result = calculator.recalculate(Currency.getInstance("USD"), BigDecimal.valueOf(3.69), Currency.getInstance("EUR"));
        Assertions.assertThat(result).isEqualByComparingTo(BigDecimal.valueOf(3.39));
    }

    @Test
    public void shouldConvertPLNtoEUR() {
        BigDecimal result = calculator.recalculate(Currency.getInstance("PLN"), BigDecimal.valueOf(10.50), Currency.getInstance("EUR"));
        Assertions.assertThat(result).isEqualByComparingTo(BigDecimal.valueOf(2.32));
    }

    @Test
    public void invalidCurrencyCode() {
        Assertions.assertThatThrownBy(() -> calculator.recalculate("Myszkamiki", BigDecimal.ZERO, "EUR"))
                .isExactlyInstanceOf(ExchangerIllegalArgumentException.class)
                .hasMessage("Invalid currency code: Myszkamiki");
    }

    @Test
    public void invalidTargetCurrencyCode() {
        Assertions.assertThatThrownBy(() -> calculator.recalculate("EUR", BigDecimal.ZERO, "Dzepetto"))
                .isExactlyInstanceOf(ExchangerIllegalArgumentException.class)
                .hasMessage("Invalid target currency code: Dzepetto");
    }

    @Test
    public void unknownRatesForCurrency() {
        exchangeTable.clear();
        Assertions.assertThatThrownBy(() -> calculator.recalculate("EUR", BigDecimal.ZERO, "PLN")).hasMessage("Unknown rates for currency: EUR");
    }

    @Test
    public void unknownRatesForTargetCurrency() {
        exchangeTable.clear();
        Map<Currency, BigDecimal> rates = new HashMap<>();
        rates.put(Currency.getInstance("GBP"), BigDecimal.valueOf(5.0538));
        exchangeTableManager.merge(rates);
        Assertions.assertThatThrownBy(() -> calculator.recalculate("GBP", BigDecimal.ZERO, "EUR")).hasMessage("Unknown rates for target currency: EUR");
    }
}