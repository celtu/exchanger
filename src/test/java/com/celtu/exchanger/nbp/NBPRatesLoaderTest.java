package com.celtu.exchanger.nbp;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
public class NBPRatesLoaderTest {

    @Autowired
    private NBPRatesLoader loader;

    private Map<Currency, BigDecimal> tableA;

    @Before
    public void init() {
        tableA = loader.loadTable("A");
    }

    @Test
    public void shouldContainManyRates() {
        Assertions.assertThat(tableA).size().isGreaterThan(1);
    }

    @Test
    public void shouldNonNullContainUSDRate() {
        BigDecimal usdRate = tableA.get(Currency.getInstance("USD"));
        Assertions.assertThat(usdRate).isGreaterThan(BigDecimal.ZERO);
    }
}