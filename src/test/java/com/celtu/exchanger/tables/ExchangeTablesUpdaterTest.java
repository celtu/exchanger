package com.celtu.exchanger.tables;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Currency;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
public class ExchangeTablesUpdaterTest {

    @Autowired
    private ExchangeTablesUpdater exchangeTablesUpdater;

    @Autowired
    private ExchangeTable exchangeTable;

    @Before
    public void init() {
        exchangeTablesUpdater.update();
    }

    @Test
    public void shouldContainEntriesFromBothTablesAB() {
        Assertions.assertThat(exchangeTable.getTable())
        .containsKeys(
                Currency.getInstance("USD"), // A
                Currency.getInstance("ZWL") // B
        );
    }

    @Test
    public void shouldNotCrashSubsequentCall() {
        exchangeTablesUpdater.update();
    }
}