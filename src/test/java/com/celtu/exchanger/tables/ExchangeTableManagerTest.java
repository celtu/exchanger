package com.celtu.exchanger.tables;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource("/test.properties")
public class ExchangeTableManagerTest {

    public static final BigDecimal EUR_RATE = BigDecimal.valueOf(4.51);
    public static final BigDecimal USD_RATE = BigDecimal.valueOf(3.91);

    @Autowired
    private ExchangeTable exchangeTable;

    @Autowired
    private ExchangeTableManager exchangeTableManager;

    @Before
    public void init() {
        Map<Currency, BigDecimal> rates = new HashMap<>();
        rates.put(Currency.getInstance("EUR"), EUR_RATE);
        rates.put(Currency.getInstance("USD"), USD_RATE);
        exchangeTableManager.merge(rates);
    }

    @Test
    public void shouldContainUsdRate() {
        Assertions.assertThat(exchangeTable.getRate(Currency.getInstance("USD"))).isEqualByComparingTo(USD_RATE);
    }

    @Test
    public void shouldContainEurRate() {
        Assertions.assertThat(exchangeTable.getRate(Currency.getInstance("EUR"))).isEqualByComparingTo(EUR_RATE);
    }

    @Test
    public void shouldAddAnotherExchangeRate() {
        Map<Currency, BigDecimal> rates = new HashMap<>();
        rates.put(Currency.getInstance("GBP"), BigDecimal.valueOf(5.02));
        exchangeTableManager.merge(rates);
        Assertions.assertThat(exchangeTable.getTable())
                .containsKeys(
                        Currency.getInstance("GBP"),
                        Currency.getInstance("EUR")
                );
    }

    @Test
    public void shoulReturn1ForPLN() {
        Assertions.assertThat(exchangeTable.getRate(Currency.getInstance("PLN"))).isEqualByComparingTo(BigDecimal.ONE);
    }

}