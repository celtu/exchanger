package com.celtu.exchanger.tables;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@TestPropertySource(value = "/test.properties", properties = {"exchanger.table.update.scheduler=true"})
public class ExchangeTableSchedulerTest {

    @MockBean
    private ExchangeTablesUpdater exchangeTablesUpdater;

    @Test
    public void shouldTriggerTablesUpdateOnStartup() throws InterruptedException {
        BDDMockito.verify(exchangeTablesUpdater, Mockito.timeout(5000).times(1)).update();
    }

}