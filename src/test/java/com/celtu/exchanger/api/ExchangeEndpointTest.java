package com.celtu.exchanger.api;

import com.celtu.exchanger.tables.ExchangeTablesUpdater;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/test.properties")
public class ExchangeEndpointTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ExchangeTablesUpdater updater;

    @Before
    public void init() {
        updater.update();
    }

    @Test
    public void validExchangeRequest() {
        ResponseEntity<BigDecimal> response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest("EUR", BigDecimal.valueOf(10), "PLN"), BigDecimal.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotZero();
    }

    @Test
    public void shouldRejestMissingBaseCurrencyCode() {
        ResponseEntity response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest(null, BigDecimal.valueOf(10), "PLN"), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldRejectMissingBaseAmount() {
        ResponseEntity response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest("EUR", null, "PLN"), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldRejectMissingTargetCurrencyCode() {
        ResponseEntity response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest("EUR", BigDecimal.valueOf(10), null), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturn400WhenInvalidCurrencyCode() {
        ResponseEntity response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest("BLABLA", BigDecimal.valueOf(10), "EUR'"), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void shouldReturn400WhenInvalidTargetCurrencyCode() {
        ResponseEntity response = restTemplate.postForEntity("/api/exchanger/", new ExchangeRequest("EUR", BigDecimal.valueOf(10), "BLABLA'"), String.class);
        Assertions.assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
    }



}