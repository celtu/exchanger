## build

mvn clean install

## then run

java -jar target/exchanger-0.0.1-SNAPSHOT.jar

## then call

curl --header "Content-Type: application/json" --request POST --data '{"currencyCode": "EUR", "amount": 10, "targetCurrencyCode": "USD"}' http://localhost:8080/api/exchanger/

## tests

please run tests
